-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2013 at 02:14 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_billing`
--
CREATE DATABASE IF NOT EXISTS `db_billing` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_billing`;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_client` varchar(10) DEFAULT NULL,
  `client_name` varchar(100) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `desktop_os` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ket` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `no_client`, `client_name`, `ip_address`, `desktop_os`, `username`, `password`, `status`, `ket`) VALUES
(1, 'R 1', 'Larry Ellison', '192.168.1.12', 'ubuntu', 'juliet', 'xubuntu', 1, ''),
(2, 'R 2', 'Mark Shuttleworth', '192.168.1.13', 'Ubuntu', '', '', 1, ''),
(3, 'R 3', 'Sergey Brin', '192.168.1.14', 'Cent Os', '', '', 0, ''),
(4, 'R 4', 'James Gosling', '192.168.1.15', 'madriva', 'gosling', 'gosling', 0, ''),
(5, 'R 5', 'Jonathan Ive', '192.168.1.16', 'Windows', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_laporan`
--

CREATE TABLE IF NOT EXISTS `detail_laporan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `laporan_id` int(11) NOT NULL,
  `service` varchar(255) NOT NULL,
  `qty` double DEFAULT NULL,
  `biaya` double NOT NULL,
  `ket` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `detail_laporan`
--

INSERT INTO `detail_laporan` (`id`, `laporan_id`, `service`, `qty`, `biaya`, `ket`) VALUES
(4, 12, 'Akses Internet (00 hr : 07 min : 36 sec)', NULL, 1250, NULL),
(3, 11, 'Akses Internet (00 hr : 00 min : 08 sec)', NULL, 0, NULL),
(5, 13, 'Akses Internet (00 hr : 19 min : 59 sec)', NULL, 2500, NULL),
(6, 14, 'Akses Internet (00 hr : 08 min : 47 sec)', NULL, 1250, NULL),
(7, 11, 'Nasi Goreng', 2, 30000, NULL),
(8, 16, 'Aqua', 4, 16000, NULL),
(9, 11, 'Print (B/W)', 2, 6000, NULL),
(10, 17, 'Print (B/W)', 2, 6000, NULL),
(11, 16, 'Nasi Goreng', 2, 30000, NULL),
(12, 16, 'Akses Internet (02 hr : 01 min : 20 sec)', NULL, 11250, NULL),
(13, 15, 'Teh Botol', 12, 60000, NULL),
(14, 17, 'Akses Internet (02 hr : 28 min : 59 sec)', NULL, 12500, NULL),
(15, 15, 'Akses Internet (02 hr : 53 min : 50 sec)', NULL, 15000, NULL),
(16, 19, 'Nasi Goreng', 1, 15000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE IF NOT EXISTS `laporan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `operator_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`id`, `client_id`, `customer_name`, `start_time`, `end_time`, `operator_id`, `status`) VALUES
(11, 1, NULL, '2013-09-17 17:50:59', '2013-09-17 17:51:07', 1, 1),
(12, 1, NULL, '2013-10-15 17:55:03', '2013-10-15 18:02:39', 1, 1),
(13, 2, NULL, '2013-10-15 18:01:18', '2013-10-15 18:21:17', 1, 1),
(14, 1, NULL, '2013-10-15 18:21:05', '2013-10-15 18:29:52', 1, 1),
(15, 5, NULL, '2013-10-15 18:21:54', '2013-10-15 21:15:44', 1, 1),
(16, 3, NULL, '2013-10-15 18:22:02', '2013-10-15 20:23:22', 1, 1),
(17, 1, NULL, '2013-10-15 18:46:41', '2013-10-15 21:15:40', 1, 1),
(18, 1, NULL, '2013-10-15 21:21:08', '0000-00-00 00:00:00', 1, 0),
(19, 2, NULL, '2013-10-16 00:00:38', '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `operator`
--

CREATE TABLE IF NOT EXISTS `operator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `address` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `operator`
--

INSERT INTO `operator` (`id`, `username`, `password`, `full_name`, `address`, `status`, `last_login`) VALUES
(1, 'gandalf', 'ab17850978e36aaf6a2b8808f1ded971', 'admin', NULL, 1, '2013-10-15 04:43:24'),
(2, 'mithrandir', 'd5210b1458e95cd4e55bf76eb9381895', 'Mithrandir', '', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE IF NOT EXISTS `paket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_paket` varchar(100) NOT NULL,
  `waktu` double NOT NULL,
  `tarif` double NOT NULL,
  `ket` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id`, `nama_paket`, `waktu`, `tarif`, `ket`) VALUES
(1, 'Paket 3 Jam', 3, 10000, ''),
(2, 'Paket  5 Jam', 5, 20000, '');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `harga_jual` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ket` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `nama`, `harga_jual`, `status`, `ket`) VALUES
(1, 'Nasi Goreng', 15000, 1, ''),
(2, 'Aqua', 4000, 1, ''),
(3, 'Print (B/W)', 3000, 1, 'Per Lembar'),
(4, 'Print (Color)', 1000, 1, 'Per Lembar'),
(5, 'Teh Botol', 5000, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `ket` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `key`, `value`, `ket`) VALUES
(1, 'nama_warnet', 'Ligos', ''),
(2, 'alamat_warnet', 'Jl. Tubagus Ismail 23 Bandung', ''),
(3, 'tarif_internet', '5000', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
