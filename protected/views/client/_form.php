<?php
/* @var $this ClientController */
/* @var $model Client */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'client_name'); ?>
		<?php echo $form->textField($model,'client_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'client_name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'no_client'); ?>
		<?php echo $form->textField($model,'no_client',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'no_client'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ip_address'); ?>
		<?php echo $form->textField($model,'ip_address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ip_address'); ?>
	</div>
	
	<fieldset style="background: #EFEFEF">
		<legend><strong><h4>Login Information</h4></strong></legend>
		
		<div class="row">
			<?php echo $form->labelEx($model,'desktop_os'); ?>
			<?php echo $form->textField($model,'desktop_os',array('size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'desktop_os'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
	</fieldset>
<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
*/ ?>
	<div class="row">
		<?php echo $form->labelEx($model,'ket'); ?>
		<?php echo $form->textArea($model,'ket',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ket'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->