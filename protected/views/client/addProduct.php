<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	//'Clients'=>array('index'),
	'Add Product & Service',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#client-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	'enableAjaxValidation'=>false,
        'method'=>'post',
)); ?>
<h2>Tambahkan Other Product/Service Ke Client <?php echo Client::model()->findByPk($model->client_id)->no_client;?></h2>
<div class="row">
Pilih Product/Service :
<select name="service" id="service">
    <option value=""> -- Product/Service --</option>
    <?php foreach(Service::model()->findAll() as $data){?>
        <option value="<?php echo $data->id;?>"><?php echo $data->nama ?></option>
    <?php }?>
</select><br/><br/>
</div>
<div class="row">
Jumlah : <?php echo CHtml::textField('qty');?>
</div>
<br /><br />
<div class="row buttons">
        <?php echo CHtml::submitButton('Tambahkan'); ?>
</div>

<?php $this->endWidget(); ?>