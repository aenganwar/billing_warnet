<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	//'Clients'=>array('index'),
	'Billing',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#client-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Dashboard Billing</h1>
<p>- Tarif Internet <b>Rp. <?php echo number_format(Setting::model()->findByAttributes(array('key'=>tarif_internet))->value,2);?>/1 Jam</b></p>
<p>- Tarif Minimal <b><?php echo number_format(Setting::model()->findByAttributes(array('key'=>tarif_internet))->value/4,2) ?></b> (untuk lima belas menit pertama)</p>
<?php echo CHtml::link('Refresh Page',Yii::app()->baseUrl.'/index.php?r=/client/billing', array('id'=>'refresh')); ?><br /><br/>
<div class="timer-panel">
    <?php echo date("j F Y")?>  - Time. <span class="timer"></span> <?php echo date("A");?>
</div>

<table class="billing">
    <thead>
        <tr>
            <th width="9%">Operations</th>
            <th width="8%">Client No</th>
            <th width="13%">Client Name</th>
            <th width="12%">Start Time</th>
            <th width="13%">Durasi</th>
            <th width="10%">Biaya Internet (Rp.)</th>
	    <th width="10%">Biaya Lainnya (Rp.)</th>
	    <th width="10%">Total (Rp.)</th>
            <th width="5%">Status</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($model as $key=>$data){?>
            <?php if($data->status ==0){?>
            <tr>
                <td style="text-align: center">
                    <a href="<?php echo Yii::app()->baseUrl.'/index.php?r=client/request&clientId='.$data->id;?>">
                    <img width="20px" title="Start Client" style="margin:0 0px 0px 0;" src="<?php echo Yii::app()->request->baseUrl.'/images/play.png'?>"></a>
                </td>
                <td style="text-align: center"><?php echo $data->no_client;?></td>
                <td><?php echo $data->client_name;?></td>
                <td style="text-align: center">-</td>
                <td style="text-align: center">-</td>
                <td style="text-align: center">-</td>
		<td style="text-align: center">-</td>
		<td style="text-align: center">-</td>
                <td style="text-align: center"><i class="label hijau">Kosong</i></td>
            </tr>
	    <!-- Client Terisi-->
        <?php }else{ ?>
              <tr>
                <td style="text-align: center">
                    <a href="<?php echo Yii::app()->baseUrl.'/index.php?r=client/stopAkses&clientId='.$data->id;?>">
                    <img width="20px" title="Stop Client" style="margin:0 0px 0px 0;" src="<?php echo Yii::app()->request->baseUrl.'/images/stop.png'?>"></a>
		    
		    <a href="<?php echo Yii::app()->baseUrl.'/index.php?r=client/addProduct&clientId='.$data->id;?>">
		    <img width="20px" title="Add Other Product & Service" style="margin:0 0px 0px 0;" src="<?php echo Yii::app()->request->baseUrl.'/images/plus.png'?>"></a>
		    
		</td>
                <td style="text-align: center"><?php echo $data->no_client;?></td>
                <td><?php echo $data->client_name;?></td>
                <td style="text-align: center">
		    <?php echo Tools::getTime(Laporan::model()->findByAttributes(array('client_id'=>$data->id, 'status'=>0))->start_time)?>    
                </td>
                <td style="text-align: center">
		    <span id="durasi"><?php echo Tools::getDurasi($data->id, date("Y-m-d H:i:s"))?></span>
		</td>
                <td style="text-align: right">
		    Rp. <?php
			    $biayaInternet = Tools::getBiayaInternet($data->id);
			    echo number_format($biayaInternet);
			?>,-
		</td>
		<td style="text-align: right">
		    Rp. <?php
			    $otherBiaya = Client::model()->getOtherIncome(Laporan::model()->findByAttributes(array('client_id'=>$data->id, 'status'=>0))->id );
			    echo number_format($otherBiaya);
			?>,-
		</td>
		<td style="text-align: right">
		    <b>Rp. <?php echo number_format(($biayaInternet + $otherBiaya));?>,-</b>
		</td>
                <td style="text-align: center"><i class="label merah">Terisi</i></td>
            </tr>  
        <?php
            }
        }?>
    </tbody>
</table>


