<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
	<script language="JavaScript" type="text/JavaScript">

	function timer()
	{
		var d = new Date();  
		var jam = d.getHours();
		var menit = d.getMinutes();
		var detik = d.getSeconds();
		var strwaktu = (jam<10)?"0"+jam:jam;
		
		strwaktu +=(menit<10)?":0"+menit:":"+menit;
		strwaktu +=(detik<10)?":0"+detik:":"+detik;
		$(".timer").html(strwaktu);
		setTimeout("timer()",200);
		
		Number.prototype.formatMoney = function(c, d, t){
		var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		};
	}	
	</script>
</head>

<body onload="timer()">

<div class="container" id="page">

	<div id="header">
		<div id="logo">
			<img width="80px" style="margin:0 5px -25px 0;" src="<?php echo Yii::app()->request->baseUrl.'/images/welcome.png'?>">
			<?php echo CHtml::encode(Yii::app()->name); ?>
			<?php echo Setting::model()->findByAttributes(array('key'=>'nama_warnet'))->value;?>
		</div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Dashboard', 'url'=>array('/client/billing'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Billing', 'url'=>array('/client/billing'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Client', 'url'=>array('/client/admin'), 'visible'=>!Yii::app()->user->isGuest),		
				//array('label'=>'Paket', 'url'=>array('/paket/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Product & Service', 'url'=>array('/service/admin'), 'visible'=>!Yii::app()->user->isGuest),				
				array('label'=>'Setting', 'url'=>array('/setting/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Operator', 'url'=>array('/operator/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Report', 'url'=>array('/laporan/admin'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Digitak Labs.<br/>
		All Rights Reserved.<br/>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
