<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	//'Clients'=>array('index'),
	'Laporan',
);

?>

<h1>Income Report</h1>

<div style="padding5px; margin: 10px">
	
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'post',
)); ?>

	<b>Filter Data :</b>
    <fieldset style="padding: 15px;margin-top:5px; ">
	<b>Bulan :</b>
		<select name="bulan" class="span2">
			<option value="">Pilih Bulan</option>
			<option value="1" <?php if($bulan=='1') echo "selected" ?>>Januari</option>
			<option value="2" <?php if($bulan=='2') echo "selected" ?>>Februari</option>
			<option value="3" <?php if($bulan=='3') echo "selected" ?>>Maret</option>
			<option value="4" <?php if($bulan=='4') echo "selected" ?>>April</option>
			<option value="5" <?php if($bulan=='5') echo "selected" ?>>Mei</option>
			<option value="6" <?php if($bulan=='6') echo "selected" ?>>Juni</option>
			<option value="7" <?php if($bulan=='7') echo "selected" ?>>Juli</option>
			<option value="8" <?php if($bulan=='8') echo "selected" ?>>Agustus</option>
			<option value="9" <?php if($bulan=='9') echo "selected" ?>>September</option>
			<option value="10" <?php if($bulan=='10') echo "selected" ?>>Oktober</option>
			<option value="11" <?php if($bulan=='11') echo "selected" ?>>Nopember</option>
			<option value="12" <?php if($bulan=='12') echo "selected" ?>>Desember</option>
		</select>
	<b>Tahun : </b>
		<?php $tahun_opt=Laporan::model()->findAll(array('condition'=>'status=1','select'=>'Distinct YEAR(end_time) AS end_time'));?>
		<select name="tahun" class="span2">
			<option value="0">Pilih Tahun</option>
			<?php foreach ($tahun_opt as $key=>$val):?>
				<option value='<?php echo $val->end_time; ?>' <?php echo $val->end_time == $tahun ? "selected" : "" ?>><?php echo $val->end_time; ?></option>";
			<?php endforeach; ?>
		</select>
		
		<?php echo CHtml::submitButton('Search'); ?>
				
		
    </fieldset>
<?php $this->endWidget(); ?>
</div>

<table class="billing">
    <thead>
        <tr>
	    <th rowspan="2" width="5%">No</th>
            <th rowspan="2" width="10%">Client No</th>
            <th rowspan="2" >Client Name</th>
            <th rowspan="2" >Tanggal</th>
            <th rowspan="2" >Durasi</th>
            <th rowspan="1" colspan="2">Income</th>
	    <th rowspan="2">Total (Rp.)</th>
	    <th rowspan="2">Operator</th>
        </tr>
	<tr>
		<th>Internet (Rp.)</th>
		<th>Other (Rp.)</th>
	</tr>
    </thead>
    <tbody>
        <?php foreach($model as $key=>$data){?>
              <tr>
                <td style="text-align: center"><?php echo $key+1;?></td>
                <td style="text-align: center"><?php echo Client::model()->findByPk($data->client_id)->no_client;?></td>
                <td><?php echo Client::model()->findByPk($data->client_id)->client_name;?></td>
                <td style="text-align: center">
		    <?php 
			list($date, $time) = explode(' ', $data->end_time);
			echo Tools::getMonth($date);
		    ?>    
                </td>
                <td style="text-align: right">
		    <?php echo Tools::getDurasiLaporan($data->id)?>  
		</td>
                <td style="text-align: right; background: #FFFFAA">
		   <?php $internet = DetailLaporan::model()->findByAttributes(array('laporan_id'=>$data->id, 'qty'=>null))->biaya;
			echo number_format($internet,2);
			$totalInternet += $internet;
		   ?>,-
		</td>
		<td style="text-align: right; background: #FFFFAA">
		    <?php
			$otherBiaya = Client::model()->getOtherIncome($data->id);
			echo number_format($otherBiaya);
			$totalOther += $otherBiaya;
		    ?>,-
		</td>
		<td style="text-align: right; background: #FFFFAA">
			<?php $subTotal = $internet + $otherBiaya;
			echo number_format($subTotal);
			$total += $subTotal;
			?>,-
		</td>
		<td><?php echo Operator::model()->findByPk($data->operator_id )->full_name;?></td>
            </tr>  
        <?php }?>
	<tr style="background: #DDD">
		<td colspan="5" style="font-weight: bold; text-align: right">TOTAL (Rp.)</td>
		<td style="text-align: right; font-weight: bold">Rp. <?php echo number_format($totalInternet,2);?></td>
		<td style="text-align: right; font-weight: bold">Rp. <?php echo number_format($totalOther, 2);?></td>
		<td style="text-align: right; font-weight: bold">Rp. <?php echo number_format($total, 2);?></td>
		<td></td>
	</tr>
    </tbody>
</table>

