<?php

/**
 * This is the model class for table "paket".
 *
 * The followings are the available columns in table 'paket':
 * @property integer $id
 * @property string $nama_paket
 * @property double $waktu
 * @property double $tarif
 * @property string $ket
 */
class Paket extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Paket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paket';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_paket, waktu, tarif', 'required'),
			array('waktu, tarif', 'numerical'),
			array('nama_paket', 'length', 'max'=>100),
			array('ket', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_paket, waktu, tarif, ket', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_paket' => 'Nama Paket',
			'waktu' => 'Waktu',
			'tarif' => 'Tarif',
			'ket' => 'Ket',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_paket',$this->nama_paket,true);
		$criteria->compare('waktu',$this->waktu);
		$criteria->compare('tarif',$this->tarif);
		$criteria->compare('ket',$this->ket,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}