<?php

/**
 * This is the model class for table "detail_laporan".
 *
 * The followings are the available columns in table 'detail_laporan':
 * @property integer $id
 * @property integer $laporan_id
 * @property string $service
 * @property double $qty
 * @property double $biaya
 * @property string $ket
 */
class DetailLaporan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetailLaporan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detail_laporan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('laporan_id, service, biaya', 'required'),
			array('laporan_id', 'numerical', 'integerOnly'=>true),
			array('qty, biaya', 'numerical'),
			array('service', 'length', 'max'=>255),
			array('ket', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, laporan_id, service, qty, biaya, ket', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'laporan_id' => 'Laporan',
			'service' => 'Service',
			'qty' => 'Qty',
			'biaya' => 'Biaya',
			'ket' => 'Ket',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('laporan_id',$this->laporan_id);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('biaya',$this->biaya);
		$criteria->compare('ket',$this->ket,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}