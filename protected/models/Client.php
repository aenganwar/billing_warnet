<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $id
 * @property string $client_name
 * @property string $ip_address
 * @property string $desktop_os
 * @property integer $status
 * @property string $ket
 */
class Client extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_name, ip_address, desktop_os', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('client_name, no_client, username, password, ip_address, desktop_os', 'length', 'max'=>100),
			array('ket', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, client_name, no_client, username, password, ip_address, desktop_os, status, ket', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_name' => 'Client Name',
			'no_client' => 'Client No',
			'ip_address' => 'Ip Address',
			'desktop_os' => 'Desktop Os',
			'username' => 'Login Username',
			'password' => 'Login Password',
			'status' => 'Status',
			'ket' => 'Ket',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_name',$this->client_name,true);
		$criteria->compare('no_client',$this->no_client,true);
		$criteria->compare('ip_address',$this->ip_address,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('desktop_os',$this->desktop_os,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('ket',$this->ket,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getOtherIncome($laporanId)
	{
		$sql = "SELECT * FROM detail_laporan WHERE laporan_id = $laporanId AND qty IS NOT NULL";
		$model = DetailLaporan::model()->findAllBySql($sql);
		foreach($model as $data)
		{
			$income += $data->biaya;
		}
		
		return $income;
	}
}