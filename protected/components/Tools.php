<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Tools {
    public static function number($number,$decimals=0){
		$result = number_format($number,$decimals,'.',',');
		
		return $result;
	}
	
    public static function getMonth($date, $model=1)
    {
	    list($year, $month, $day) = explode('-', $date);
	    
	    if($model==1)
	    {
		$month = Tools::getMonthName($month);
		
		$date = $day."-".$month."-".$year;
	    }
	    elseif($model==2)
	    {
		$date = $day."-".$month."-".$year;
	    }		
	    return $date;
    }
    
    public static function getMonthName($month)
    {
	switch ($month){
	    case '01' : $month = 'Jan'; break;
	    case '02' : $month = 'Feb'; break;
	    case '03' : $month = 'March'; break;
	    case '04' : $month = 'Apr'; break;
	    case '05' : $month = 'Mei'; break;
	    case '06' : $month = 'Jun'; break;
	    case '07' : $month = 'Jul'; break;
	    case '08' : $month = 'Augt'; break;
	    case '09' : $month = 'Sept'; break;
	    case '10' : $month = 'Okt'; break;
	    case '11' : $month = 'Nov'; break;
	    case '12' : $month = 'Dec'; break;
	}
	
	return $month;
    }
    
    public static function getNamaHari($hari){
	
	switch($hari){     
	    case 0 : {
			$hari='Sun';
		    }break;
	    case 1 : {
			$hari='Mon';
		    }break;
	    case 2 : {
			$hari='Tue';
		    }break;
	    case 3 : {
			$hari='Wen';
		    }break;
	    case 4 : {
			$hari='Thu';
		    }break;
	    case 5 : {
			$hari="Fri";
		    }break;
	    case 6 : {
			$hari='Sat';
		    }break;
	    default: {
			$hari='UnKnown';
		    }break;
	}
	
	return $hari;
    }
    
    
    public static function getTime($stamp)
    {
	list($date, $time) = explode(' ', $stamp);
	
	return $date.' - '.$time;
    }
    
    public static function getDurasi($clientId, $timeEnd, $status = 0)
    {
	$timeStamp = Laporan::model()->findByAttributes(array('client_id'=>$clientId, 'status'=>$status))->start_time;
	list($date, $time, ) = explode(' ', $timeStamp);
	list($Enddate, $Endtime, ) = explode(' ', $timeEnd);
	
	$a = explode(":", $time);       
        $b = explode(":", $Endtime);
	
	/* Explode parameter $time_1 */
        $a_hour    = $a[0];
        $a_minutes = $a[1];
        $a_seconds = $a[2];
         
        /* Explode parameter $time_2 */
        $b_hour    = $b[0];
        $b_minutes = $b[1];
        $b_seconds = $b[2];
	
	/* declare result variabel */       
        $c_hour    = NULL;
        $c_minutes = NULL;
        $c_seconds = NULL;
	
	/* -----------------------------------------
        * Pengurangan detik
        * -----------------------------------------
        **/
        if($b_seconds >= $a_seconds)
        {
            $c_seconds = $b_seconds - $a_seconds;
        }
        else
        {
            $c_seconds = ($b_seconds + 60) - $a_seconds;
            $b_minutes--;
        }       
         
        /* -----------------------------------------
        * Pengurangan menit
        * -----------------------------------------
        **/
        if($b_minutes >= $a_minutes)
        {
            $c_minutes = $b_minutes - $a_minutes;
        }
        else
        {
            $c_minutes = ($b_minutes + 60) - $a_minutes;
            $b_hour--;
        }       
         
        /* -----------------------------------------
        * Pengurangan jam
        * -----------------------------------------
        **/
        if($b_hour >= $a_hour)
        {
            $c_hour = $b_hour - $a_hour;
        }
        else
        {
            $c_hour = '-' . ($a_hour - $b_hour);
        }
	
	/* Checking time format */
        if( strlen($c_seconds) == 1) $c_seconds = '0'.$c_seconds;
        if( strlen($c_minutes) == 1) $c_minutes = '0'.$c_minutes;
        if( strlen($c_hour) == 1) $c_hour = '0'.$c_hour;
         
        /* Return result */
        return $c_hour . ' hr : ' . $c_minutes . ' min : ' . $c_seconds.' sec';
    }
    
    public static function getDurasiLaporan($id)
    {
	$timeStampStart = Laporan::model()->findByPk($id)->start_time;
	$timeStampEnd = Laporan::model()->findByPk($id)->end_time;
	
	list($date, $time) = explode(' ', $timeStampStart);
	list($endDate, $endTime) = explode(' ', $timeStampEnd);
	
	$a = explode(":", $time);       
        $b = explode(":", $endTime);
	
	/* Explode parameter $time_1 */
        $a_hour    = $a[0];
        $a_minutes = $a[1];
        $a_seconds = $a[2];
         
        /* Explode parameter $time_2 */
        $b_hour    = $b[0];
        $b_minutes = $b[1];
        $b_seconds = $b[2];
	
	/* declare result variabel */       
        $c_hour    = NULL;
        $c_minutes = NULL;
        $c_seconds = NULL;
	
	/* -----------------------------------------
        * Pengurangan detik
        * -----------------------------------------
        **/
        if($b_seconds >= $a_seconds)
        {
            $c_seconds = $b_seconds - $a_seconds;
        }
        else
        {
            $c_seconds = ($b_seconds + 60) - $a_seconds;
            $b_minutes--;
        }       
         
        /* -----------------------------------------
        * Pengurangan menit
        * -----------------------------------------
        **/
        if($b_minutes >= $a_minutes)
        {
            $c_minutes = $b_minutes - $a_minutes;
        }
        else
        {
            $c_minutes = ($b_minutes + 60) - $a_minutes;
            $b_hour--;
        }       
         
        /* -----------------------------------------
        * Pengurangan jam
        * -----------------------------------------
        **/
        if($b_hour >= $a_hour)
        {
            $c_hour = $b_hour - $a_hour;
        }
        else
        {
            $c_hour = '-' . ($a_hour - $b_hour);
        }
	
	/* Checking time format */
        if( strlen($c_seconds) == 1) $c_seconds = '0'.$c_seconds;
        if( strlen($c_minutes) == 1) $c_minutes = '0'.$c_minutes;
        if( strlen($c_hour) == 1) $c_hour = '0'.$c_hour;
         
        /* Return result */
        return $c_hour . ' hr : ' . $c_minutes . ' min : ' . $c_seconds.' sec';
    }
    
    public static function getBiayaInternet($clientId)
    {
	$time = Tools::getDurasi($clientId, date("Y-m-d H:i:s"));
	$a = explode(":", $time);       
	
	/* Explode parameter $time_1 */
        $a_hour    = $a[0];
        $a_minutes = $a[1];
        $a_seconds = $a[2];
		
	$tarif = Setting::model()->findByAttributes(array('key'=>tarif_internet))->value;
	$biaya = ceil($tarif / 4);
	
	$durasiMinutes = $a_hour * 60 + $a_minutes;
    
	return ceil($durasiMinutes/15)*$biaya;
    }
    
    public static function getSettingApp($key)
    {
	$settingApp = SettingAplikasi::model()->findByAttributes(array('key'=>$key));
	
	return $settingApp->isi;
    }
    
}
?>